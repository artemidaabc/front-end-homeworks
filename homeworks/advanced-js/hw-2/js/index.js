const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function createList(book) {
    const bookItem = document.createElement("li");
    const propertyList = document.createElement("ul");
    const root = document.querySelector('#root');
    const bookList = document.createElement("ul");
    root.append(bookList);

    for (const property in book) {
        const propertyItem = document.createElement("li");
        propertyItem.textContent = book[property];
        propertyList.append(propertyItem);
    }

    bookItem.append(propertyList);
    bookList.append(bookItem);
}

function errorSearch(item) {
    item.forEach(book => {
        try {
            ["author", "name", "price"].forEach(property => {
                if (!book.hasOwnProperty(property)) {
                    throw new Error(`${property} is not found`);
                }
            });

            createList(book);
        } catch (e) {
            console.error(e);
        }
    });
}

errorSearch(books);

