import React from 'react';
import './App.scss'
import Header from "./components/Header/Header";
import NavMenu from "./components/NavMenu/NavMenu";
import AppRoutes from "./routes/AppRoutes";
import connect from "react-redux/lib/connect/connect";
import {cartSelector} from "./redux/cart/cartSelector";
import {addToCartModal} from "./components/Modal/templates/addToCart";
import {removeModal} from "./components/Modal/templates/removeFromCart";



const App = (props) => {

  return (
      <div className="App">
        <Header title='online toy store' logo={'logo.png'} nav ={<NavMenu/>}/>

        <AppRoutes />

        { props.popup.name === 'add-to-cart' && addToCartModal() }
        { props.popup.name === 'remove-from-cart' && removeModal() }

      </div>
  )
}

const mapStoreToProps = (store) => ({
  cart: cartSelector(store),
  popup: {...store.popup},
})


export default connect(mapStoreToProps)(App);
