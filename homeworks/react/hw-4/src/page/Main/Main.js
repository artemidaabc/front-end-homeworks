import React, { useEffect } from 'react';
import store from "../../redux/storeConfig";
import './Main.scss'
import Catalog from "../../components/Catalog/Catalog";

import { getGoods } from "../../redux/goods/goodsActions";

const Main = () => {

  useEffect(() => {
    store.dispatch(getGoods())
  }, [])

  const goods = store.getState().goods;

  return (
    <div className="main">
      {goods.length > 0 && <Catalog />}
    </div>
  );
};

export default Main;