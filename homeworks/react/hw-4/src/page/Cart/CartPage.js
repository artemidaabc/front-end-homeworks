import React from 'react';
import Cart from "../../components/Cart/Cart";
import store from "../../redux/storeConfig";
import EmptyCart from "../../components/Cart/EmptyCart/EmptyCart";

const CartPage = () => {

  const cart = store.getState().cart

  return (
    <div className={'cart-wrapper'}>
      {cart.length === 0 && <EmptyCart />}
      {cart.length > 0 && <Cart />}
    </div>
  );
};

export default CartPage;