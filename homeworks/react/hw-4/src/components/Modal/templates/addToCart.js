import React from "react";
import Actions from "../Actions/Actions";
import Button from "../../Button/Button";
import store from "../../../redux/storeConfig";
import { addToCart } from "../../../redux/cart/cartActions";
import Modal from "../Modal";
import { hideModalWindow } from "../../../redux/modals/modalActions";

export const addToCartModal = () => {
  const props = {
    header: `Do you want to add ${store.getState().popup.item.name} to cart?`,
    text: `You have ${store.getState().cart.length} items in cart`,
    modifier: 'cart',
    actions: <Actions buttons={[
      <Button key="0"
        text="Add"
        handler={() => store.dispatch(addToCart(store.getState().popup.item))}
        modifier="cart-modal" />,
      <Button key="1"
        text="Close"
        handler={(ev) => store.dispatch(hideModalWindow(ev))}
        modifier="cart-modal close" />]}
    />
  }

  return <Modal {...props} />
}