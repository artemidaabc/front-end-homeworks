import React from 'react';
import CloseButton from "./CloseButton/CloseButton";
import "./Modal.scss"
import PropTypes, { string } from 'prop-types';
import { hideModalWindow } from "../../redux/modals/modalActions";
import connect from "react-redux/lib/connect/connect";

const Modal = (props) => {

  const { header,
    closeWindow,
    closeButton,
    text,
    actions,
    modifier
  } = props;

  return (
    <div className={"modal"}
      onClick={(ev) => closeWindow(ev)}
    >
      <div className={`modal__content modal__content--${modifier}`}>
        <div className={`modal__header modal__header--${modifier}`}>
          <p className={`modal__header-text modal__header-text--${modifier}`}>
            {header}
          </p>
          {closeButton && <CloseButton close={(ev) => closeWindow(ev)} />}
        </div>

        <div className={`modal__description modal__description--${modifier}`}>
          <p className={`modal__description-text modal__description-text--${modifier}`}>
            {text}
          </p>
        </div>
        {actions}
      </div>
    </div>
  );
}

const mapDispatchToProps = dispatch => ({
  closeWindow: (ev) => dispatch(hideModalWindow(ev))
})

export default connect(null, mapDispatchToProps)(Modal);

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired,
  close: PropTypes.func,
  modifier: string
}

Modal.defaultProps = {
  modifier: 'std',
  close: null
}