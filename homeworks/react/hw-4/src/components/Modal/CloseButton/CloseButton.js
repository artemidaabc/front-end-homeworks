import React from 'react';
import './CloseButton.scss';
import PropTypes from 'prop-types';

const CloseButton = (props) => {
  const { close } = props;
  return <div className={"close-btn"} onClick={(ev) => close(ev)} />
};

export default CloseButton;

CloseButton.propTypes = {
  close: PropTypes.func
}

CloseButton.defaultProps = {
  close: null
}