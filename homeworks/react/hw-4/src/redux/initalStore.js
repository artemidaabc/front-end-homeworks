const initialStore = {
  popup: {
    name: null,
    item: {
      name: null,
      vendorCode: null,
      price: null,
      color: null,
      image: null,
    }
  },
  goods: [],
  favorites: JSON.parse(localStorage.getItem('favorites')) || [],
  cart: JSON.parse(localStorage.getItem('cart')) || [],
}

export default initialStore;