import { combineReducers } from "redux";
import favoritesReducer from "./favorites/favoritesReducer";
import goodsReducer from "./goods/goodsReducer";
import cartReducer from "./cart/cartReducer";
import modalReducer from "./modals/modalReducer";

export default combineReducers({
  goods: goodsReducer,
  cart: cartReducer,
  favorites: favoritesReducer,
  popup: modalReducer
})