import { CART_ADD, CART_REMOVE } from "../actionTypes";

export const addToCart = (item) => ({
  type: CART_ADD,
  item
})

export const removeFromCart = (item) => ({
  type: CART_REMOVE,
  item
})