import { React, Component } from 'react';
import './Modal.scss';

class Modal extends Component {

    render() {
        const { header, text, actions, closeButton, onClick } = this.props;

        return (
            <>
                <div className="modal__container">
                    {closeButton ? <button className={"modal__container-closeBtn"} onClick={onClick}>X</button> : ''}
                    <h2 className={"modal__container-title"}>{header}</h2>
                    <p className={"modal__container-text"}>{text}</p>
                    <div className={'modal__buttons'}>{actions}</div>
                </div>
                <div className={"modal-wrapper"} onClick={onClick} />
            </>
        );
    }
}

export default Modal;
