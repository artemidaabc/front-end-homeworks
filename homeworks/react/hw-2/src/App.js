import React, { Component } from 'react';
import './App.css'
import Header from "./components/Header/Header";
import Catalog from "./components/Catalog/Catalog";
import Modal from "./components/Modal/Modal";
import Actions from "./components/Modal/Actions/Actions";
import Button from "./components/Button/Button";

class App extends Component {
  state = {
    popupIsActive: false,
    goods: [],
    favorites: JSON.parse(localStorage.getItem('favorites')) || [],
    cart: JSON.parse(localStorage.getItem('cart')) || [],
    currentItem: {
      name: null,
      vendorCode: null
    }
  }

  componentDidMount() {
    fetch('goods.json')
      .then(res => res.json())
      .then(data => {
        this.setState({
          goods: data
        })
      });
  }

  render() {
    const { popupIsActive, cart, currentItem } = this.state
    return (
      <div className="App">
        <Header title="sport motorcycles"
          logo={"logo.png"} />
        <Catalog goods={this.state.goods}
          favorites={this.state.favorites}
          addToFav={this.toggleFav}
          showModal={this.renderCartModal} />
        {popupIsActive ? <Modal
          header={`Do you want to add ${currentItem.name} to cart?`}
          text={`You have ${cart.length} items in cart `}
          closeButton={true}
          close={this.toggleModal}
          modifier="cart"
          actions={<Actions buttons={[
            <Button key="0" text="Add" handler={() => this.addToCart(currentItem.vendorCode)} modifier="cart-modal" />,
            <Button key="1" text="Close" handler={this.toggleModal} modifier="cart-modal" />]
          } />}
        /> : void 0}
      </div>
    )
  }

  toggleFav = (vendorCode) => {
    const { favorites } = this.state;
    if (!favorites.includes(vendorCode)) {

      favorites.push(vendorCode);
    } else {
      favorites.splice(favorites.indexOf(vendorCode), 1)
    }
    this.setState({
      favorites: favorites
    })
    localStorage.setItem('favorites', JSON.stringify(favorites))
  }

  addToCart = (vendorCode) => {
    const { cart } = this.state;
    cart.push(vendorCode);
    this.setState({
      cart: cart
    })
    localStorage.setItem('cart', JSON.stringify(cart));
    this.toggleModal();
  }

  toggleModal = () => {
    this.setState({
      popupIsActive: !this.state.popupIsActive
    })
  }

  setCurrentItem(props) {
    this.setState({
      currentItem: {
        name: props.name,
        vendorCode: props.vendorCode
      }
    })
  }

  renderCartModal = (props) => {
    this.setCurrentItem(props);
    this.toggleModal();
  }

}

export default App;
