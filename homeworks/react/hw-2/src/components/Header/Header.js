import React, { Component } from 'react';
import './Header.scss';
import PropTypes from 'prop-types';

class Header extends Component {
  render() {
    const { title, logo, buttons } = this.props
    return (
      <header className="header">
        <div className="header__logo">
          <img src={logo} alt="company name" />
        </div>
        <h1 className="header__title">{title}</h1>
        {buttons}
      </header>
    );
  }
}
export default Header;

Header.propTypes = {
  title: PropTypes.string,
  logo: PropTypes.string,
  buttons: PropTypes.array
}

Header.defaultProps = {
  buttons: null
}
