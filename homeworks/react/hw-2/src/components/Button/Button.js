import React, { Component } from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends Component {
   render() {
      const { text, handler, modifier } = this.props;

      return (
         <button className={`btn btn--${modifier}`} onClick={handler}>{text}</button>
      );
   }
}

export default Button;

Button.propTypes = {
   text: PropTypes.string,
   handler: PropTypes.func,
   modifier: PropTypes.string
}

Button.defaultProps = {
   modifier: 'std'
}
