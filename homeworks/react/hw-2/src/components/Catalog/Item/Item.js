import React, { Component } from 'react';
import './Item.scss'
import Button from "../../Button/Button";
import StarIcon from "./StarIcon/StarIcon";
import PropTypes from 'prop-types';


class Item extends Component {

  render() {
    const { name, price, image, vendorCode, color, addToFav, showModal, favorites } = this.props;
    const isFavorite = favorites.includes(this.props.vendorCode);

    return (
      <div className="product-item">
        <div className="product-item__image"><img src={image} alt={`product code: ${vendorCode}`} /></div>
        <h4 className="product-item__title">{name}</h4>
        <div className="product-item__description">
          <span className="product-item__code">{`vendor code: ${vendorCode}`}</span>
          <span className="product-item__color">{`color: ${color}`}</span>

          <span className="product-item__price">{"price: "}
            <span className="product-item__price-sum">{price}</span></span>
        </div>
        <StarIcon indicated={isFavorite} itemID={vendorCode} clickHandler={addToFav} />
        <Button modifier="cart" text="Add to cart" handler={() => showModal({ ...this.props })} />
      </div>
    );
  }
}

export default Item;

Item.propTypes = {
  name: PropTypes.string,
  price: PropTypes.string,
  image: PropTypes.string,
  vendorCode: PropTypes.number,
  color: PropTypes.string,
  favorites: PropTypes.array,
  addToFav: PropTypes.func,
  showModal: PropTypes.func,
}
