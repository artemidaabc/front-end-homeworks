import initialStore from "../../initalStore";
import { CART_ORDER, CART_SET_CONSUMER } from "../../actionTypes";

export default (store = initialStore.order, { type, payload }) => {

  if (type === CART_ORDER) {
    return {
      ...store,
      ...payload
    }

  } else if (type === CART_SET_CONSUMER) {
    return {
      ...store,
      consumer: {
        ...store.consumer,
        ...payload
      }
    }

  } else {
    return store
  }
}