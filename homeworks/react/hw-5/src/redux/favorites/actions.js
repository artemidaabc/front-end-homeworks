import { FAVORITES_TOGGLE } from "../actionTypes";

export const addToFavorite = (item) => ({
  type: FAVORITES_TOGGLE,
  payload: item
})
