import React from 'react';
import './Cart.scss'
import CartItem from "./CartItem/CartItem";
import PropTypes from 'prop-types';
import { selector } from "../../redux/cart/selector";
import connect from "react-redux/lib/connect/connect";
import { activateRemoveModal } from "../../redux/modals/actions";
import OrderForm from "./OrderForm/OrderForm";
import getCartItems from "../../utils/getUniqueProductsArray";

const Cart = (props) => {
  const { cart, removeAction } = props;
  const items = getCartItems(cart);

  const renderedItems = items.map(item => {
    let { name, price, image, vendorCode, color, quantity } = item;
    return <CartItem name={name}
      price={price}
      image={image}
      vendorCode={vendorCode}
      color={color}
      quantity={quantity}
      removeAction={removeAction}
      key={vendorCode} />
  });

  return (
    <div className="cart">
      {renderedItems}
      <OrderForm />
    </div>
  );
}

const mapStoreToProps = (store) => ({
  cart: selector(store)
});

const mapDispatchToProps = dispatch => ({
  removeAction: (itemProps) => dispatch(activateRemoveModal(itemProps))
})

export default connect(mapStoreToProps, mapDispatchToProps)(Cart);

Cart.propTypes = {
  cart: PropTypes.array.isRequired,
  removeAction: PropTypes.func.isRequired,
}