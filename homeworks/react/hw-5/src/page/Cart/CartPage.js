import React from 'react';
import Cart from "../../components/Cart/Cart";
import store from "../../redux/storeConfig";
import EmptyCart from "../../components/Cart/EmptyCart/EmptyCart";

const CartPage = () => {

  const cart = store.getState().cart

  return (
    <div className={'cart-wrapper'}>
      {!cart.length && <EmptyCart />}
      {cart.length && <Cart />}
    </div>
  );
};

export default CartPage;