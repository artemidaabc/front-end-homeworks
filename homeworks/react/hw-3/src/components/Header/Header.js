import React from 'react';
import './Header.scss';
import PropTypes from 'prop-types';

const Header = (props) => {

  const { title, logo, nav } = props;
  return (
    <header className="header">
      <div className="header__logo">
        <div className="header__logo-picture">
          <img src={logo} alt="company name" />
        </div>
        <h1>{title}</h1>
      </div>
      {nav}
    </header>
  );
}

export default Header;

Header.propTypes = {
  title: PropTypes.string,
  logo: PropTypes.string,
  nav: PropTypes.object
}

Header.defaultProps = {
  nav: null
}