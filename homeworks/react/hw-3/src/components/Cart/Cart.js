import React from 'react';
import './Cart.scss'
import CartItem from "./CartItem/CartItem";
import PropTypes from 'prop-types';

const Cart = (props) => {
  const { cart, removeAction } = props;
  //get unique array of items inside cart with quantity of each
  const cartItems = () => {
    const duplicate = cart.map(item => {
      item.quantity = cart.filter(el => el.vendorCode === item.vendorCode).length
      return item;
    });
    const codes = [...new Set(duplicate.map(item => item.vendorCode))]

    return codes.map(code => duplicate.find(item => item.vendorCode === code));
  }

  const items = cartItems();

  const renderedItems = items.map(item => {
    let { name, price, image, vendorCode, color, quantity } = item;
    return <CartItem name={name}
      price={price}
      image={image}
      vendorCode={vendorCode}
      color={color}
      quantity={quantity}
      removeAction={removeAction}
      key={vendorCode} />
  });

  return (
    <div className="cart">
      {renderedItems}
    </div>
  );
}

export default Cart;

Cart.propTypes = {
  cart: PropTypes.array,
  removeAction: PropTypes.func,
}