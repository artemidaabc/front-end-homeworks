import React from 'react';
import { Link } from "react-router-dom";
import './NavMenu.scss'

const NavMenu = () => {
  return (
    <nav className="navbar">
      <Link className="navbar__link" to={"/"}>Home</Link>
      <Link className="navbar__link" to={"/cart"}>Cart</Link>
      <Link className="navbar__link" to={"/favorites"}>Favorites</Link>
    </nav>
  )
};

export default NavMenu;