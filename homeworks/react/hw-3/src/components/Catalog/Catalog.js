import React from 'react';
import Item from "./Item/Item";
import './Catalog.scss'
import PropTypes from 'prop-types';

const Catalog = (props) => {

  const { goods, addToFav, showModal, favorites } = props;
  const items = goods.map(item => {
    let { name, price, image, vendorCode, color } = item;
    return <Item name={name}
      price={price}
      image={image}
      vendorCode={vendorCode}
      color={color}
      showModal={showModal}
      addToFav={addToFav}
      favorites={favorites}
      key={vendorCode} />
  });

  return (
    <div className="catalog">
      {items}
    </div>
  );
}

export default Catalog;

Catalog.propTypes = {
  goods: PropTypes.array,
  favorites: PropTypes.array,
  addToFav: PropTypes.func,
  showModal: PropTypes.func
}
