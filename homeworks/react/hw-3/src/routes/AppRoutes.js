import React from 'react';
import Catalog from "../components/Catalog/Catalog";
import Cart from "../components/Cart/Cart"
import { Switch, Route } from 'react-router-dom'


const AppRoutes = (props) => {
  const { catalogProps, cartProps, favProps } = props;

  return (
    <>
      <Switch>
        <Route exact path={'/'} render={() => <Catalog {...catalogProps} />} />
        <Route exact path={'/favorites'} render={() => <Catalog {...favProps} />} />
        <Route exact path={'/cart'} render={() => <Cart {...cartProps} />} />
      </Switch>
    </>
  );
};

export default AppRoutes;