import React from 'react';
import Favorites from "../page/Favorites/Favorites";
import { Switch, Route, Redirect} from 'react-router-dom'
import CartPage from "../page/Cart/CartPage";
import Main from "../page/Main/Main";



const AppRoutes = () => {

  return (
    <>
      <Switch>
        <Route exact path="/" component={Main}/>
        <Route exact path="/cart" component={CartPage}/>
        <Route exact path="/favorites" component={Favorites}/>}/>
        <Route exact path="/*"><Redirect to="/"/></Route>
      </Switch>
    </>
  );
};

export default AppRoutes;