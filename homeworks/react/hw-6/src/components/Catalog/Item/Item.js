import React from 'react';
import './Item.scss'
import Button from "../../Button/Button";
import StarIcon from "./StarIcon/StarIcon";
import PropTypes from 'prop-types';

const Item = (props) => {

  const { name, price, image, vendorCode, color, addToFav, showModal, isFavorite } = props;

  return (
    <div className="product-item">
      <div className="product-item__image">
        <img src={image} alt={`product code: ${vendorCode}`} />
      </div>
      <h4 className="product-item__title">{name}</h4>
      <div className="product-item__description">
        <span className="product-item__code">{`vendor code: ${vendorCode}`}</span>
        <span className="product-item__color">{`color: ${color}`}</span>

        <span className="cart-item__price">{"price: "}
          <span className="cart-item__price-sum">{price} &#36;
          </span>
        </span>
      </div>
      <StarIcon indicated={isFavorite}
        item={{ name, price, color, image, vendorCode, isFavorite }}
        clickHandler={addToFav} />
      <Button modifier="cart" text="Add to cart" handler={() => showModal({ ...props })} />
    </div>
  );
}

export default Item;


Item.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  image: PropTypes.string,
  vendorCode: PropTypes.number,
  color: PropTypes.string,
  isFavorite: PropTypes.bool,
  addToFav: PropTypes.func,
  showModal: PropTypes.func,
}