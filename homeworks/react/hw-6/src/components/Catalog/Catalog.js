import React, { useEffect } from 'react';
import Item from "./Item/Item";
import './Catalog.scss'
import PropTypes from 'prop-types';
import { selector as favSelector } from "../../redux/favorites/selector";
import { selector as goodsSelector } from "../../redux/goods/selector";
import connect from "react-redux/lib/connect/connect";
import { addToFavorite } from "../../redux/favorites/actions";
import { activateCartModal } from "../../redux/modals/actions";
import { getGoods } from "../../redux/goods/actions";

const Catalog = (props) => {

  const { goodsFromStore, getGoods, showCartModal, favorites, renderedFavorites, toggleFav, selector } = props;

  useEffect(getGoods, []);

  let itemsToRender;

  if (selector === 'favorites') {
    itemsToRender = renderedFavorites

  } else {
    itemsToRender = goodsFromStore;
  }

  const items = itemsToRender.map(item => {
    const { name, price, image, vendorCode, color } = item;
    const isFavorite = favorites.some(el => el.vendorCode === item.vendorCode)

    return <Item name={name}
      price={price}
      image={image}
      vendorCode={vendorCode}
      color={color}
      showModal={showCartModal}
      isFavorite={isFavorite}
      addToFav={toggleFav}
      key={vendorCode} />
  });
  return (
    <div className="catalog">
      {items}
    </div>
  );
}

const mapStoreToProps = (store, ownProps) => ({
  favorites: favSelector(store),
  goodsFromStore: goodsSelector(store),
  ...ownProps
});

const mapDispatchToProps = dispatch => ({
  toggleFav: (item) => dispatch(addToFavorite(item)),
  showCartModal: (props) => dispatch(activateCartModal(props)),
  getGoods: () => dispatch(getGoods())
})

export default connect(mapStoreToProps, mapDispatchToProps)(Catalog);

Catalog.propTypes = {
  goodsFromStore: PropTypes.array.isRequired,
  favorites: PropTypes.array.isRequired,
  toggleFav: PropTypes.func.isRequired,
  showCartModal: PropTypes.func.isRequired
}

Catalog.defaultProps = {
  selector: 'all'
}