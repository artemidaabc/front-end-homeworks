import React from 'react';
import './Header.scss';
import PropTypes from 'prop-types';

const Header = (props) => {
  const { title, logo, nav } = props;
  return (
    <header className="header">
      <div className="header__logo">
        <div className="header__logo-picture">
          <img src={logo} alt="company name" />
        </div>
        <h1>{title}</h1>
      </div>
      {nav}
    </header>
  );
}

export default Header;

Header.propTypes = {
  title: PropTypes.string.isRequired,
  logo: PropTypes.string.isRequired,
  nav: PropTypes.object.isRequired
}

Header.defaultProps = {
  nav: null
}