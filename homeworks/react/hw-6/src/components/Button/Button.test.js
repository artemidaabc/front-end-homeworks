import React from "react";
import Button from "./Button";
import '@testing-library/jest-dom'

describe('Unit testing for Button component', () => {

  test('smoke test for Button', () => {
    render(<Button/>);
  });

  describe('Button renders children', () => {

    test('text from props is rendered correctly', () => {
      const targetText = "Don't click me ever",
        {getByTestId} = render(<Button text={targetText}/>),
        btnText = getByTestId('buttonComponent').textContent;

      expect(btnText).toBe(targetText);
    });
    test('HTML from props is rendered', () => {
      const targetElement = <span data-testid="test-component">any strange things could be here</span>,
        {getByTestId} = render(<Button element={targetElement}/>),
        testElement = getByTestId('test-component');

      expect(testElement).toBeInTheDocument();
    });
  })

  test('Button recevies correct type from props', () => {
    const targetType = "submit",
      {getByTestId} = render(<Button type={targetType}/>);

      expect(getByTestId('buttonComponent').type).toBe(targetType);
  });

  test('Button calls handler on click', () => {
    const testHandler = jest.fn(),
      {getByTestId} = render(<Button handler={testHandler}/>);
      click(getByTestId('buttonComponent'));

      expect(testHandler).toHaveBeenCalledTimes(1);
  });

  test('Button className includes modifier from props', () => {
    const testModifier = 'testing'
    const {getByTestId} = render(<Button modifier={testModifier}/>);
    const btn = getByTestId('buttonComponent');

    expect(btn.className).toContain(`--${testModifier}`);
  });

  test('Button remains disabled due to props value', () => {
    const testHandler = jest.fn(),
      {getByTestId} = render(<Button disabled={true} handler={testHandler}/>);
    click(getByTestId('buttonComponent'));

    expect(getByTestId('buttonComponent')).toBeDisabled();
    expect(testHandler).not.toHaveBeenCalled();
  });
})

