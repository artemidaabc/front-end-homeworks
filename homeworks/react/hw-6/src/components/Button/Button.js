import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

const Button = (props) => {
  const { text, element, type, handler, modifier, disabled } = props;
  return (
    <button data-testid="buttonComponent" className={`btn btn--${modifier}`} type={type} disabled={disabled} onClick={handler}>{text || element}</button>
  );
}

export default Button;

Button.propTypes = {
  handler: PropTypes.func,
  text: PropTypes.string,
  type: PropTypes.string,
  modifier: PropTypes.string,
  element: PropTypes.object,
  disabled: PropTypes.bool
}

Button.defaultProps = {
  handler: null,
  modifier: 'std',
  element: null,
  text: null,
  type: null,
  disabled: false
}