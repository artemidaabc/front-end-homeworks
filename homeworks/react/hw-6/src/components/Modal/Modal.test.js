import React from "react";
import { Modal } from "./Modal";


describe('Unit test for Modal component', () => {
  test('smoke test', () => {
    render(<Modal actions={<div/>}
                  text={''}
                  header={''}
                  closeButton={false}
                  />);
  });

    test('text from props is rendered', () => {
      const headerTestText = 'this is header';
      const mainTestText = 'this is main text';
      const {getByText} = render(<Modal actions={<div/>}
                                        text={mainTestText}
                                        header={headerTestText}
                                        closeButton={false}
                                  />);
      expect(getByText(headerTestText).textContent).toBe(headerTestText);
      expect(getByText(mainTestText).textContent).toBe(mainTestText);
    });

    test('close button appears due to props', () => {
      jest.mock("./CloseButton/CloseButton");

      const {getByTestId} = render(<Modal actions={<div/>}
                                          text={''}
                                          header={''}
                                          closeButton={true}
                                    />);

      expect(getByTestId('closeBtn')).toBeInTheDocument();
    });

    test('action element appears due to props', () => {

      const ActionComponent = () => {
        return (
          <div data-testid = "actionTests"/>
        )
      }

      const {getByTestId} = render(<Modal actions={<ActionComponent/>}
                                          text={''}
                                          header={''}
                                          closeButton={false}
      />);

      expect(getByTestId('actionTests')).toBeInTheDocument();
    });

    test('modifier is set correctly form props', () => {

      const modifier = 'test';

      const {getByTestId} = render(<Modal actions={<div/>}
                                          text={''}
                                          header={''}
                                          closeButton={true}
                                          modifier={modifier}
      />);

      expect(getByTestId('modalContent')).toHaveClass(`modal__content--${modifier}`)
    })
});