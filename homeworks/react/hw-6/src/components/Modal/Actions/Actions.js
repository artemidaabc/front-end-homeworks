import React from 'react';
import "./Actions.scss";
import PropTypes from 'prop-types';

const Actions = (props) => {
  const { buttons } = props;
  return (
    <div className="actions">
      {buttons}
    </div>
  );
}

export default Actions;

Actions.propTypes = {
  buttons: PropTypes.array,
}
Actions.defaultProps = {
  buttons: null,
}
