import React from "react";
import Actions from "../Actions/Actions";
import Button from "../../Button/Button";
import store from "../../../redux/storeConfig";
import { addToCart } from "../../../redux/cart/actions";
import Modal from "../Modal";
import { hideModalWindow } from "../../../redux/modals/actions"

export const addToCartModal = () => {
  const quantity = store.getState().cart.length;

  const props = {
    header: `${quantity
      ? `Do you want to add more ${store.getState().popup.item.name} to cart?`
      : 'Please confirm your choice'}`,

    text: `${quantity
      ? `You have ${quantity} items in cart`
      : `Do you want to add ${store.getState().popup.item.name} to cart?`}`,
    closeButton: true,
    modifier: 'cart',
    actions: <Actions buttons={[
      <Button key="0"
        text="Add"
        handler={() => store.dispatch(addToCart(store.getState().popup.item))}
        modifier="cart-modal" />,
      <Button key="1"
        text="Close"
        handler={(ev) => store.dispatch(hideModalWindow(ev))}
        modifier="cart-modal close" />]}
    />
  }

  return <Modal {...props} />
}