import {CART_ORDER, CART_SET_CONSUMER} from "../../actionTypes";

export const createOrder = (orderData) => {
    return {
        type: CART_ORDER,
        payload: {
            ...orderData,
        }
    }
    };

export const setConsumer = (data) => ({
    type: CART_SET_CONSUMER,
    payload: data
})
