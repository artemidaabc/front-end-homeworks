import {MODAL_ADD_TO_CART, MODAL_HIDE, MODAL_REMOVE_FROM_CART} from "../actionTypes";

export function activateCartModal(itemProps) {
  const { name,vendorCode,price,color,image } = itemProps;
  return {
    type: MODAL_ADD_TO_CART,
    item: { name,vendorCode,price,color,image }
  }
}

export function activateRemoveModal(itemProps) {
  const {name, vendorCode, price, color, image} = itemProps;
  return {
    type: MODAL_REMOVE_FROM_CART,
    item: {name, vendorCode, price, color, image}
  }
}

export function hideModalWindow(ev) {
    return {
      type: MODAL_HIDE,
      class: ev.target.className,
      attribute: ev.target.hasAttribute('close')
    }
}
