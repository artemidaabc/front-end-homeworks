import axios from "axios";
import {GOODS_LOADED} from "../actionTypes";

export const getGoods = () => {
  return(dispatch) => {
    axios('goods.json').then(res => {
      dispatch({
        type: GOODS_LOADED,
        payload: res.data
      });
    })
  }
}