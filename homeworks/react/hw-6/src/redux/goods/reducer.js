import initialStore from "../initalStore";
import {GOODS_LOADED} from "../actionTypes";

export default function (goods = initialStore.goods,{type, payload}) {
  switch (type) {
    case GOODS_LOADED:
      return payload
    default: return goods;
  }

}