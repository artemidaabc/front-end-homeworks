import {combineReducers} from "redux";
import favoritesReducer from "./favorites/reducer";
import goodsReducer from "./goods/reducer";
import cartReducer from "./cart/reducer";
import modalReducer from "./modals/reducer";
import orderReducer from "./cart/order/reducer";

export default combineReducers ({
  goods: goodsReducer,
  cart: cartReducer,
  favorites: favoritesReducer,
  popup: modalReducer,
  order: orderReducer
})