import '@testing-library/jest-dom';
import '@testing-library/jest-dom/extend-expect';
import { render,fireEvent } from '@testing-library/react'

global.render = render;
global.click = fireEvent.click;