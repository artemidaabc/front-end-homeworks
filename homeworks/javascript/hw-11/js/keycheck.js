document.addEventListener("keydown", (event) => {
    for (let key of document.getElementsByClassName("btn-wrapper")[0].children) {
        key.style.backgroundColor = "#33333a";
        if (key.innerText.toUpperCase() === event.key.toUpperCase()) {
            key.style.backgroundColor = "teal";
        }
    }
});