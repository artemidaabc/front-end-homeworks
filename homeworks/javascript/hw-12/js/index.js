let imageIndex = 0;
let images = document.getElementsByClassName("image-to-show");

function updateImage() {
    for (let image of images) {
        image.style.display = "none";
    }
    images[imageIndex].style.display = "flex";
    imageIndex = (imageIndex + 1) % images.length;
}

let cycle = null;

function startCycle() {
    if (cycle === null) {
        cycle = setInterval(updateImage, 3000);
    }
}

function stopCycle() {
    clearInterval(cycle);
    cycle = null;
}

updateImage();
startCycle();