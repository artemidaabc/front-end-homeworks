function createNewUser() {
    return {
        firstName: prompt('First name:'),
        lastName: prompt('Last name:'),
        birthday: prompt('Birthday (year.month.day):'),
        getAge: function () {
            let now = new Date();
            if (this.birthday.split('.')[0] > now.getFullYear()) {
                return 'Wrong year';
            }
            else if (this.birthday.split('.')[1] > now.getMonth()) {
                return 'Wrong mouth';
            }
            else if (this.birthday.split('.')[2] > now.getDay()) {
               return 'Wrong day';
            }
            else {
                return 'Your age: '+ (now.getFullYear() - this.birthday.split('.')[0]);
            }
        },
        getPassword: function () {
            return (this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase()) + (this.birthday.split('.')[0]);
        }
    };
}
let user = createNewUser();

console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

