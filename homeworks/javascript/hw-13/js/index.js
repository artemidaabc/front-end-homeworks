function updateMode() {
    document.getElementById("color-mode").href = localStorage.dark === "true" ? "css/dark.css" : "css/light.css";
    document.getElementById("color-mode-link").innerText = localStorage.dark === "true" ? "Light mode" : "Dark mode";
}

function toggleMode() {
    localStorage.dark = localStorage.dark === "false" ? "true" : "false";
    updateMode();
}

localStorage.dark = localStorage.dark ? localStorage.dark : "false";
updateMode();