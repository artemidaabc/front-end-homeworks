function createList() {
    let array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
    let list = document.createElement('ul');
    document.body.append(list);

    list.append(...array.map(text => {
        let listItem = document.createElement('li');
        listItem.append(text);
        return listItem;
    }));
}

createList();