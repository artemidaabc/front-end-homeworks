for (let eye of document.getElementsByClassName("icon-password")) {
    eye.onclick = () => {
        let shouldShowPassword = eye.classList.contains("fa-eye-slash");
        if (shouldShowPassword) {
            eye.classList.remove("fa-eye-slash");
            eye.classList.add("fa-eye");
        }
        else {
            eye.classList.remove("fa-eye");
            eye.classList.add("fa-eye-slash");
        }
        for (let sibling of eye.parentElement.children) {
            if (sibling.tagName === "INPUT") {
                sibling.type = shouldShowPassword ? "text" : "password";
            }
        }
    };
}

document.getElementsByClassName("btn")[0].onclick = () => {
    let inputs = document.getElementsByTagName("input");
    if (inputs[0].value === inputs[1].value && inputs[0].value !=='' && inputs[1].value !=='') {
        document.getElementsByClassName("error-text")[0].style.display = "none";
        alert("You are welcome");
    }
    else {
        document.getElementsByClassName("error-text")[0].style.display = "flex";
    }
};