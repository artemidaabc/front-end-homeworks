let priceSpan = document.createElement('span');
priceSpan.style.display = 'block';

let clearButton = document.createElement('button');
clearButton.append(document.createTextNode('X'));

let errorSpan = document.createElement('span');
errorSpan.innerText = 'Please enter correct price.';
errorSpan.style.display = 'block';

let input = document.getElementsByTagName('input')[0];
input.onfocus = function () {
    input.style.outline = 'none';
    input.style.border = '1px solid green';
};
input.onblur = function () {
    if (input.value >= 0) {
        input.style.border = '1px solid black';
        input.style.color = 'green';
        priceSpan.innerText = 'Current price: ' + input.value;
        document.body.append(clearButton);
        errorSpan.remove();
        document.body.prepend(priceSpan);
    }
    else {
        input.style.border = '1px solid red';
        input.style.color = 'red';
        document.body.append(errorSpan);
        priceSpan.remove();
    }
    if (input.value === "") {
        priceSpan.innerText = '';
    }
};
clearButton.onclick = function () {
    priceSpan.innerText = '';
    errorSpan.remove();
    input.value = '';
};
