function updateTabContent() {
    for (let item of document.getElementsByClassName("tabs-content")[0].children) {
        item.hidden = true;
    }
    for (let tab of document.getElementsByClassName("navbar-services")[0].children) {
        if (tab.classList.contains("focus")) {
            document.getElementsByClassName(tab.innerHTML)[0].hidden = false;
        }
    }
}

updateTabContent();

document.getElementsByClassName("navbar-services")[0].onclick = function (event) {
    document.getElementsByClassName("focus")[0].classList.remove("focus");
    event.target.classList.add("focus");
    updateTabContent();
};