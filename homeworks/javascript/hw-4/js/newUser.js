function createNewUser() {
    return {
        firstName: prompt("First name:"),
        lastName: prompt("Last name:"),
        getLogin: function() {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        }
    };
}

let user = createNewUser();

console.log(user);
console.log(user.getLogin());
